$(function() {

//Настраиваем instafeed
    var feed = new Instafeed({
        clientId: '495ac0fa21fc437c9b1d65cfa400798f',
        target: 'portfolio-loading',
        get: 'tagged',
        tagName: 'programming',
        links: true,
        limit: 3,
        sortBy: 'most-recent',
        resolution: 'low_resolution',
        template: '<div class="col-md-8 portfolio-item"><a href="{{link}}"><img src="{{image}}"></a><div class="portfolio-item__header">{{model.date}}</div><p>{{caption}}</p></div>',
        success: function() {
            $('.portfolio__loader').hide();
        },
    });
    feed.run();

});