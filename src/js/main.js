/*
 * Third party
 */
//= ../../bower_components/jquery/dist/jquery.min.js
//= ../../bower_components/bootstrap-sass/assets/javascripts/bootstrap.js
//= ../../bower_components/instafeed.js/instafeed.js

/*
 * Custom
 */
//= partials/helper.js
//= partials/instafeed-load.js
//= partials/common.js